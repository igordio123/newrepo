const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const http = require('http');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');
const indexRouter = require('./routes');
const usersRouter = require('./routes/usersRouter');


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/newDb', { useNewUrlParser: true }, () => {
  // eslint-disable-next-line
  console.log('hello ,i am at mongo');
});

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res) => {
  res.send(err);
});

const server = http.createServer(app);
const port = process.env.PORT || 3000;
server.listen(port, () => {
  // eslint-disable-next-line
  console.log(`listen in port: ${port}`);
});


module.exports = app;
