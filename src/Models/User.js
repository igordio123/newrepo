class User {
  constructor(data) {
    this.id = data.id;
    this.name = data.name;
    this.userName = data.userName;
    this.email = data.email;
  }
}

module.exports = User;
