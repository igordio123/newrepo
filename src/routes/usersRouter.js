const router = require('express').Router();
const userController = require('../controllers/UsersController');

/*
@oas [get] /
description: "Organization search"
tags:
  - users
summary: Find all users
operationId: Find All
responses:
  200:
    description: OK
    content:
      application/json:
        schema:
          type: object
          properties:
            id:
              type: string
              example: 5b912bdb78a1c553aa871b4e
            name:
              type: string
              example: Igor
            username:
              type: string
              example: igordio
            email:
              type: string
              example: igordio@inbox.ru

  500:
    description: Error

*/
router.get('/', userController.getAllUsers);


/*
@oas [post] /create
description: Enter your data to create new user
tags:
  - users
requestBody:
  required: true
  content:
    application/json:
      schema:
        type: object
        properties:
          name:
            type: string
          userName:
            type: string
          email:
            type: string
responses:
  200:
    description: all is OK
  500:
    description: something wrong
 */


router.post('/create', userController.createUser);

/*
@oas [delete] /{id}
description: "Delete user"
tags:
  - users
summary: Delete User!
operationId: opa
parameters:
  - (path) id=1552412440271 {String} Put your id
responses:
  200:
    description: OK
  500:
    description: Error

*/


router.delete('/:id', userController.delete);

/*
@oas [put] /{id}
description: Enter data to edit user
tags:
  - users
parameters:
  - (path) id=1 {String} Put your id
requestBody:
  required: true
  content:
    application/json:
      schema:
        type: object
        properties:
          name:
            type: string
          userName:
            type: string
          email:
            type: string
responses:
  200:
    description: all is OK

  500:
    description: something wrong
*/

router.put('/:id', userController.edit);


/*
@oas [get] /{id}
description: "Users"
tags:
  - users
summary: Find User!
operationId: opg
parameters:
  - (path) id=1552504340466 {String} Put your id
responses:
  200:
    description: OK
  500:
    description: Error

*/

router.get('/:id', userController.findById);


module.exports = router;
