const userService = require('../services/userService');

class UserController {
  static getAllUsers(req, res, next) {
    userService.findAllUsers()
      .then((users) => {
        res.send(users);
      })
      .catch(next);
  }

  static createUser(req, res, next) {
    const id = new Date().getTime();
    const { name, userName, email } = req.body;
    userService.createUser(id, name, userName, email)
      .then((result) => {
        res.send(result);
      })
      .catch(next);
  }

  static delete(req, res, next) {
    const { id } = req.params;
    userService.deleteUser(id)
      .then(() => {
        res.send(`Пользователь с id:${id}удалён`);
      })
      .catch(next);
  }

  static edit(req, res, next) {
    const el = req.body;
    const { id } = req.params;
    userService.editUser(id, el).then((result) => {
      res.send(result);
    })
      .catch(next);
  }


  static findById(req, res, next) {
    const { id } = req.params;
    userService.findUserById(id).then((result) => {
      res.send(result);
    })
      .catch(next);
  }
}

module.exports = UserController;
