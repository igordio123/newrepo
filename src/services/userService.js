const usersDAO = require('../schema/UserSchema');
const Users = require('../Models/User');


class UserService {
  static async createUser(id, name, userName, email) {
    return usersDAO.create({
      id, name, userName, email,
    });
  }

  static async findAllUsers() {
    return usersDAO.find().then(result => result.map(element => new Users(element)));
  }

  static async findUserById(id) {
    return usersDAO.findOne({ id });
  }

  static async deleteUser(id) {
    return usersDAO.deleteOne({ id });
  }

  static async editUser(id, data) {
    return usersDAO.updateOne({ id }, data);
  }
}


module.exports = UserService;
