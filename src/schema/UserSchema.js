const mongoose = require('mongoose');

const UserSchema = mongoose.Schema;


const users = new UserSchema({
  id: {
    type: Number,

  },
  name: {
    type: String,
  },
  userName: {
    type: String,
    required: [
      true, 'Укажите имя',
    ],
  },
  email: {
    type: String,
    required: [
      true, 'Укажите email',
    ],
  },
});

const Users = mongoose.model('Users', users);

module.exports = Users;
